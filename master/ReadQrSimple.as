﻿package 
{	
	import com.logosware.event.QRdecoderEvent;
	import com.logosware.event.QRreaderEvent;
	import com.logosware.utils.QRcode.QRdecode;
	import com.logosware.utils.QRcode.GetQRimage;
	
	import net.hires.debug.Stats;
	
	import com.bit101.components.*;
	import flash.media.Sound;
	import flash.display.*;
	import flash.media.Camera;
	import flash.media.Video;
	import flash.events.*;
	import flash.utils.Timer;
	import flash.desktop.NativeApplication;
	import flash.geom.Rectangle;
	
	public class ReadQrSimple extends Sprite 
	{	
		private var detectionRate = 10;
		
		private var getQRimage:GetQRimage;
		private var qrDecode:QRdecode;
		
		//string to save first decoded
		private var qrDecodedDataOld:String; 
		
		private var cam:Camera;
		private var video:Video;
		
		private var markerArea;
		private var markerAreaRect:Rectangle;
		var holder:Holder = new Holder();
		private var markerGuide:Sprite;
		private var txt:TextArea;
		private var refreshTimer:Timer;
		private var timesCompleted = 0;
		private var btnclear:BtnClear = new BtnClear();
		private var amnt_mc:amnt = new amnt();
		private var pass_mc:PassScr = new PassScr();
		private var success_mc:Success = new Success();
		private var quit:Quit = new Quit();
		private var ok_btn:Ok = new Ok();
		private var cancel_btn:Cancel = new Cancel();
		private var ok_btn1:Ok = new Ok();
		private var cancel_btn1:Cancel = new Cancel();
		private var ok_btn2:Ok = new Ok();
		private var cancel_btn2:Cancel = new Cancel();
		var snd:Sound = new camerasound();
		var readSuccess:Number = 0;
		
		public function ReadQrSimple():void 
		{	
			displayDebugText();
									
			// initialize the webcam			
			cam = Camera.getCamera();
			
			if(cam != null)
			{	
				cam.setMode(320,240,30);
				video = new Video(cam.width, cam.height);
				
				holder.x = 240;
				holder.y = 290;
				holder.rotation = 90;
				holder.scaleX = 1.3;
				holder.scaleY = 1.3;
				addChild(holder);
				
				video.attachCamera(cam);
				video.x = -160;
				video.y = -120;
				video.scaleX = 1;
				video.scaleY = 1;
				holder.addChild(video);
				
				btnclear.x= 70;
				btnclear.y = 770;
				addChild(btnclear);
				btnclear.buttonMode = true;
				
				quit.x= 250;
				quit.y = 770;
				
				addChild(quit);
				quit.buttonMode = true;
				quit.addEventListener(MouseEvent.CLICK, QuitApplication);
				
				markerArea = 200;
				markerAreaRect = new Rectangle((cam.width-markerArea)/2,(cam.height-markerArea)/2,markerArea,markerArea);
				
				getQRimage = new GetQRimage(video);
				
				// run onQrImageReadComplete when the QR Code is found 
				getQRimage.addEventListener(QRreaderEvent.QR_IMAGE_READ_COMPLETE, onQrImageReadComplete);
				
				// setup QRdecoder object
				qrDecode = new QRdecode();
				
				// invoked when QR Code is found  
				qrDecode.addEventListener(QRdecoderEvent.QR_DECODE_COMPLETE, onQrDecodeComplete);
	
				// check the image n times a second (n = detectionRate)
				refreshTimer = new Timer(1000/detectionRate); 	
				refreshTimer.addEventListener(TimerEvent.TIMER, refresh);
				refreshTimer.start();
			}
			else
			{ txt.text = "Didn't find a webcam\n";
			}
			
			// display a guide where to put the marker	
			displayMarkerGuide();
			
			// add Mr. Doobs Hi-ReS! stats 
			//addChild( new Stats() );
		}
		
		function pay(event:MouseEvent):void{
			btnclear.removeEventListener(MouseEvent.CLICK, pay);
			trace("Pay");
			amnt_mc.x= 0;
			amnt_mc.y = 0;
			addChild(amnt_mc);
			stage.focus = amnt_mc.amnt_txt;
	//		amnt_mc.amnt_txt.softKeyboardType = SoftKeyboardType.NUMBER;
            amnt_mc.amnt_txt.restrict = "0-9";
   //       amnt_mc.amnt_txt.returnKeyLabel = ReturnKeyLabel.GO;
			ok_btn.x= 70;
			ok_btn.y = 220;
			amnt_mc.addChild(ok_btn);
			ok_btn.buttonMode = true;
			cancel_btn.x= 250;
			cancel_btn.y = 220;
			amnt_mc.addChild(cancel_btn);
			amnt_mc.amnt_txt.addEventListener(Event.CHANGE, active);
			cancel_btn.addEventListener(MouseEvent.CLICK, onClear);
		}
		function active(event:Event):void{
			ok_btn.addEventListener(MouseEvent.CLICK, pass);
		}
		function pass(event:MouseEvent):void{
			ok_btn.removeEventListener(MouseEvent.CLICK, pass);
			amnt_mc.amnt_txt.text = "";
			trace ("OK");
			pass_mc.x= 0;
			pass_mc.y = 0;
			addChild(pass_mc);
			ok_btn1.x= 70;
			ok_btn1.y = 220;
			pass_mc.addChild(ok_btn1);
			ok_btn1.buttonMode = true;
			cancel_btn1.x= 250;
			cancel_btn1.y = 220;
			pass_mc.addChild(cancel_btn1);
			pass_mc.pass_txt.addEventListener(Event.CHANGE, active1);
			cancel_btn1.addEventListener(MouseEvent.CLICK, onClear2);
		}
		function active1(event:Event):void{
			ok_btn1.addEventListener(MouseEvent.CLICK, success);
		}
		function onClear(event:MouseEvent):void{
			trace ("Cancel");
			removeChild(amnt_mc);
			amnt_mc.amnt_txt.text = "";
			myQRText.text = "   ";
			readSuccess = 0;
		}
		
		function success(event:MouseEvent):void{
			ok_btn1.removeEventListener(MouseEvent.CLICK, success);
			pass_mc.pass_txt.text = "";
			trace ("Success");
			removeChild(amnt_mc);
			removeChild(pass_mc);
			success_mc.x= 0;
			success_mc.y = 0;
			addChild(success_mc);
			ok_btn2.x= 160;
			ok_btn2.y = 440;
			success_mc.addChild(ok_btn2);
			ok_btn2.buttonMode = true;
			ok_btn2.addEventListener(MouseEvent.CLICK, onClear1);
		}
		function onClear1(event:MouseEvent):void{
			trace ("Cancel");
			removeChild(success_mc);
			pass_mc.pass_txt.text = "";
			myQRText.text = "   ";
			readSuccess = 0;
		}
		
		function onClear2(event:MouseEvent):void{
			trace ("Cancel");
			removeChild(pass_mc);
			pass_mc.pass_txt.text = "";
			myQRText.text = "   ";
			readSuccess = 0;
		}
		
		private function refresh( event:Event ):void
		{	// process webcam image and check for QRcode
			getQRimage.process();
		}
		
		private function onQrImageReadComplete(e:QRreaderEvent):void
		{	
			qrDecode.setQR(e.data);
			qrDecode.startDecode();		
		}
		
		private function onQrDecodeComplete(e:QRdecoderEvent):void
		{	// e.data is the string decoded from QR Code  
			timesCompleted++; // number times of recognition since start of program
			
			var decodedDataNew = e.data;
			var compare = decodedDataNew.localeCompare(qrDecodedDataOld);
			
			if(compare==0 && readSuccess == 0)
			{ txt.text = String(timesCompleted)+"\t data: "+e.data+"\n"+txt.text;
			
			snd.play();
			myQRText.text = String(e.data);
			readSuccess = 1;
			}
			else
			{ txt.text = String(timesCompleted)+"\t ERROR \n"+txt.text;
			}
			
			qrDecodedDataOld = decodedDataNew;
			btnclear.addEventListener(MouseEvent.CLICK, pay);
		}
		
		private function displayMarkerGuide():void
		{	markerGuide = new Sprite();
			
			var guideLW:int = 5;
			var guideColor:uint = 0xFFFFFF;
			
			// make the MarkerGuide smaller than the markerArea
			// in this case users don't have to put the marker that precise in the area for detection
			var mRect = markerAreaRect;
			mRect.inflate(-40,-40)
			
			with(markerGuide.graphics) 
			{	lineStyle(guideLW, guideColor, 1, false, LineScaleMode.NONE, CapsStyle.SQUARE, JointStyle.MITER);
				drawRect(mRect.x,mRect.y,mRect.width,mRect.height);
				lineStyle();
				beginFill(guideColor);
				drawRect(mRect.x,mRect.y,10,10);
				drawRect(mRect.x+mRect.width-(guideLW*2), mRect.y,10,10);
				drawRect(mRect.x, mRect.y+mRect.height-(guideLW*2),10,10);
				endFill();
			}
			markerGuide.alpha = 0.7;
			markerGuide.scaleX = 2.6;
			markerGuide.scaleY = 2.0;
			markerGuide.x = -420;
			markerGuide.y = -240;
			
			holder.addChild(markerGuide);	
		}
		
		private function displayDebugText():void
		{	txt = new TextArea();
			
			txt.editable = false;
			txt.width = stage.stageWidth-20;
			txt.height = stage.stageHeight-700;
			txt.x=10;
			//txt.y=stage.stageHeight-txt.height-10;
			txt.y=750;
			//addChild(txt);
		}
		
		function QuitApplication(e:Event):void
		{
			trace("quit");
			holder.removeChild(video);
			cam = null;
			NativeApplication.nativeApplication.exit();
		}
	}
}